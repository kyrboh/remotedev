#pragma once

#include "sftpconnection.h"

#include <ssh/sftpsession.h>

#include <QObject>
#include <QHash>

#include <functional>

namespace QSsh { class SftpSession; }

namespace RemoteDev::Internal {

// TERMINOLOGY
// Job      a queue of actions to achieve some result, e.g.
//          upload file and create parent directories (if necessary)
// Action   a single step on the way to complete a job, e.g.
//          create directory

class SftpChannelExecutor : public QObject
{
    Q_OBJECT
public:
    using RemoteJobQueue = SftpConnection::RemoteJobQueue;
    SftpChannelExecutor(std::unique_ptr<QSsh::SftpSession> parent,
                        QHash<RemoteJobId, RemoteJobQueue *> &&actions);

signals:
    void error(const QString &reason = QString());
    void jobFinished(RemoteJobId job, RemoteJobQueue *leftoverActions);
    void jobError(RemoteJobId job, const QString &error, RemoteJobQueue *leftoverActions);

    void actionFinished(RemoteJobId job);

    void exhausted();

protected:
    friend class RemoteDev::SftpConnection;

    void startJobs();

private:
    void finishJob(RemoteJobId job, const QString &error = QString());

private slots:
    void onSessionStarted();
    void onSessionError(const QString &reason);

    void startNextAction(RemoteJobId job);
    void onCommandFinished(QSsh::SftpJobId action, const QString &error);

private:
    std::unique_ptr<QSsh::SftpSession> m_channel;
    QHash<RemoteJobId, RemoteJobQueue *> m_actions;
    QHash<QSsh::SftpJobId, RemoteJobId> m_jobs;
};

} // namespace RemoteDev::Internal

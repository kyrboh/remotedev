#pragma once

#include "connection.h"

#include <ssh/sshconnection.h>

#include <QHash>
#include <QObject>
#include <QQueue>

#include <functional>

namespace RemoteDev {

namespace Internal { class SftpChannelExecutor; }

class SftpConnection : public Connection
{
    Q_OBJECT
public:
    explicit SftpConnection(const QString &alias,
                            const QSsh::SshConnectionParameters &serverInfo,
                            QObject *parent = nullptr);

    ~SftpConnection() override;

    RemoteJobId uploadFile(Utils::FilePath local,
                           Utils::FilePath remote,
                           const Utils::FilePath &file,
                           OverwriteMode mode) override;

    RemoteJobId uploadDirectory(Utils::FilePath local,
                                Utils::FilePath remote,
                                const Utils::FilePath &directory,
                                OverwriteMode mode) override;

    RemoteJobId downloadFile(Utils::FilePath localDir,
                             Utils::FilePath remoteDir,
                             const Utils::FilePath &file,
                             OverwriteMode mode) override;

    QString errorString() const override;

private:
    friend class RemoteDev::Internal::SftpChannelExecutor;
    using RemoteJobQueue = QQueue<std::function<QSsh::SftpJobId (QSsh::SftpSession *)>>;

    static RemoteJobQueue createSubDirsActions(Utils::FilePath remoteBase,
                                               const Utils::FilePath &target);

    static RemoteJobQueue uploadDirContentsActions(Utils::FilePath local,
                                                   Utils::FilePath remote,
                                                   OverwriteMode mode);

    //! Create and run job for provided actions
    //!
    //! \note actions have to be allocated
    RemoteJobId createJob(RemoteJobQueue *actions);

private slots:
    void startJobs();
    void onSshError();

    void onUploadFinished(RemoteJobId job, RemoteJobQueue *leftovers);
    void onUploadError(RemoteJobId job, const QString &error, RemoteJobQueue *leftovers);

    void onHelperExhausted();

private:
    QSsh::SshConnection m_ssh;
    QHash<RemoteJobId, RemoteJobQueue *> m_actions;
};

} // namespace RemoteDev

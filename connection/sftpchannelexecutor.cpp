#include "connection/sftpchannelexecutor.h"

#include "common/keyiteratorproxy.h"

#include <ssh/sftpsession.h>
#include <utils/fileutils.h>

#include <QDir>

namespace RemoteDev::Internal {

SftpChannelExecutor::SftpChannelExecutor(std::unique_ptr<QSsh::SftpSession> channel,
                                         QHash<RemoteJobId, RemoteJobQueue *> &&actions)
    : QObject(),
      m_channel(std::move(channel)),
      m_actions(std::move(actions))
{
    connect(m_channel.get(), &QSsh::SftpSession::started, this, &SftpChannelExecutor::onSessionStarted);
    connect(m_channel.get(), &QSsh::SftpSession::done, this, &SftpChannelExecutor::onSessionError);

    connect(m_channel.get(),
            &QSsh::SftpSession::commandFinished,
            this,
            &SftpChannelExecutor::onCommandFinished);
}

void SftpChannelExecutor::startJobs()
{
    m_channel->start();
}

void SftpChannelExecutor::finishJob(RemoteJobId job, const QString &error)
{
    auto actions = m_actions.take(job);

    qDebug() << "SFTP" << job << "*" << ": job finished:" << error;
    if (error.isEmpty()) {
        emit jobFinished(job, actions);
    } else {
        emit jobError(job, error, actions);
    }

    if (m_actions.isEmpty()) {
        m_channel->quit();
    }
}

void SftpChannelExecutor::onSessionStarted()
{
    for (const RemoteJobId jobId : Common::KeyIteratorProxy(m_actions)) {
        startNextAction(jobId);
    }
}

void SftpChannelExecutor::onSessionError(const QString &reason)
{
    m_jobs.clear();
    m_actions.clear();

    if (!reason.isEmpty()) {
        qDebug() << "SFTP Channel Error: " << reason;
        emit error(reason);
    } else {
        emit exhausted();
    }
}

void SftpChannelExecutor::startNextAction(RemoteJobId job)
{
    auto queue = m_actions.value(job);
    if (! queue || queue->isEmpty()) {
        finishJob(job);
        return;
    }

    auto actionId = queue->dequeue()(m_channel.get());
    if (actionId != QSsh::SftpInvalidJob) {
        m_jobs.insert(actionId, job);
    } else {
        qDebug() << "SFTP" << job << actionId
                 << ": action failed: internal error";

        // ignore any errors if this is not the last action
        if (!queue->isEmpty()) {
            startNextAction(job);
        } else {
            finishJob(job, tr("internal error"));
        }
    }
}

void SftpChannelExecutor::onCommandFinished(QSsh::SftpJobId action, const QString &error)
{
    auto job = m_jobs.take(action);
    auto actions = m_actions.value(job);
    if (! actions->isEmpty()) {
        qDebug() << "SFTP" << job << action << ": action finished:" << error;
        startNextAction(job);
    } else {
        finishJob(job, error);
    }
}

} // namespace RemoteDev::Internal

#include "sftpconnection.h"

#include "sftpchannelexecutor.h"

#include <ssh/sftpsession.h>
#include <ssh/sshconnection.h>
#include <utils/fileutils.h>

#include <QDir>
#include <QFileInfo>
#include <QSharedPointer>

#include <functional>

namespace RemoteDev {

SftpConnection::SftpConnection(const QString &alias,
                               const QSsh::SshConnectionParameters &serverInfo,
                               QObject *parent)
    : Connection(alias, parent),
      m_ssh(serverInfo, parent)
{
    connect(&m_ssh, &QSsh::SshConnection::errorOccurred, this, &SftpConnection::onSshError);

    connect(&m_ssh, &QSsh::SshConnection::connected, this, &Connection::connected);

    // FIXME: handle this internally also
    connect(&m_ssh, &QSsh::SshConnection::disconnected, this, &Connection::disconnected);

    // local methods
    connect(this, &Connection::connected, this, &SftpConnection::startJobs);
}


SftpConnection::~SftpConnection()
{
    disconnect(this, SIGNAL(error()));
    disconnect(this, SIGNAL(disconnected()));
}

RemoteJobId SftpConnection::uploadFile(Utils::FilePath local,
                                       Utils::FilePath remote,
                                       const Utils::FilePath &file,
                                       OverwriteMode mode)
{
    static_cast<void>(mode);

    auto *queue = new RemoteJobQueue;
    queue->append(createSubDirsActions(remote, file));

    const auto localFile = local.pathAppended(file.toString()).toString();
    const auto remoteFile = remote.pathAppended(file.toString()).toString();
    // TODO: use static_cast<QSsh::SftpOverwriteMode>(mode)
    queue->enqueue([localFile, remoteFile](QSsh::SftpSession *session) {
        auto id = session->uploadFile(localFile, remoteFile);
        qDebug() << "SFTP"
                 << "*" << id << ": upload file:" << localFile << " -> " << remoteFile;
        return id;
    });

    return createJob(queue);
}

RemoteJobId SftpConnection::downloadFile(Utils::FilePath localDir,
                                         Utils::FilePath remoteDir,
                                         const Utils::FilePath &file,
                                         OverwriteMode mode)
{
    static_cast<void>(mode);

    const auto remoteFile = remoteDir.pathAppended(file.toString()).toString();
    const auto localFile = localDir.pathAppended(file.toString()).toString();

    // NOTE: .appendPath changes the FileName object
    if (!QDir().mkpath(localDir.parentDir().toString()))
        return REMOTE_INVALID_JOB;

    auto *actions = new RemoteJobQueue;
    // TODO: use static_cast<QSsh::SftpOverwriteMode>(mode)
    actions->enqueue([remoteFile, localFile](QSsh::SftpSession *session) {
        auto id = session->downloadFile(remoteFile, localFile);
        qDebug() << "SFTP"
                 << "*" << id << ": download file:" << remoteFile << "->" << localFile;

        return id;
    });

    return createJob(actions);
}

RemoteJobId SftpConnection::uploadDirectory(Utils::FilePath local,
                                            Utils::FilePath remote,
                                            const Utils::FilePath &directory,
                                            OverwriteMode mode)
{
    auto actions = new RemoteJobQueue;
    if (!directory.isEmpty()) {
        actions->append(createSubDirsActions(remote, directory));

        const auto localDir = local.pathAppended(directory.toString()).toString();
        const auto remoteDir = remote.pathAppended(directory.toString()).parentDir().toString();
        actions->enqueue([localDir, remoteDir](QSsh::SftpSession *channel) {
            // TODO: check id this works, previously was: channel->uploadDir(localDir, remoteDir)
            auto id = channel->uploadFile(localDir, remoteDir);
            qDebug() << "SFTP"
                     << "*" << id << ": upload directory:" << localDir << " -> " << remoteDir;
            return id;
        });
    } else {
        // relative path is empty -> have to upload contents of local to remote
        // have to work around stupid directoty upload policy of SftpChannel
        actions->append(uploadDirContentsActions(local, remote, mode));
    }

    return createJob(actions);
}

void SftpConnection::startJobs()
{
    auto channel = m_ssh.createSftpSession();
    if (!channel) {
        qDebug() << "Failed to create SFTP channel" << m_ssh.errorString();
        emit error(QStringLiteral("Failed to create SFTP channel: %1").arg(m_ssh.errorString()));
        return;
    }

    auto helper = new Internal::SftpChannelExecutor(std::move(channel), std::move(m_actions));
    m_actions.clear();

    connect(helper,
            &Internal::SftpChannelExecutor::jobFinished,
            this,
            &SftpConnection::onUploadFinished);
    connect(helper, &Internal::SftpChannelExecutor::jobError, this, &SftpConnection::onUploadError);
    connect(helper, &Internal::SftpChannelExecutor::error, this, &Connection::error);
    connect(helper, &Internal::SftpChannelExecutor::exhausted, this, &SftpConnection::onHelperExhausted);

    helper->startJobs();
}

void SftpConnection::onSshError()
{
    emit error(m_ssh.errorString());
}

void SftpConnection::onUploadFinished(RemoteJobId job, SftpConnection::RemoteJobQueue *leftovers)
{
    delete leftovers;
    emit Connection::uploadFinished(job);
}

void SftpConnection::onUploadError(RemoteJobId job,
                                   const QString &error,
                                   SftpConnection::RemoteJobQueue *leftovers)
{
    delete leftovers;
    emit Connection::uploadError(job, error);
}

void SftpConnection::onHelperExhausted()
{
    qDebug() << "Helper exhausted";
    auto *helper = qobject_cast<Internal::SftpChannelExecutor *>(QObject::sender());
    helper->deleteLater();
}

SftpConnection::RemoteJobQueue
    SftpConnection::createSubDirsActions(Utils::FilePath remoteBase, const Utils::FilePath &target)
{
    SftpConnection::RemoteJobQueue actions;

    auto parts = target.toString().split(QLatin1Char('/'));
    parts.removeLast();

    for (const auto &dir : parts) {
        const auto remoteDir = remoteBase.pathAppended(dir).toString();
        qDebug() << "Queue: create directory:" << remoteDir;

        actions.enqueue([remoteDir](QSsh::SftpSession *session) {
            auto id = session->createDirectory(remoteDir);
            qDebug() << "SFTP"
                     << "*" << id << ": create directory:" << remoteDir;
            return id;
        });
    }

    return actions;
}

SftpConnection::RemoteJobQueue SftpConnection::uploadDirContentsActions(Utils::FilePath local,
                                                                        Utils::FilePath remote,
                                                                        OverwriteMode mode)
{
    // TODO: use static_cast<QSsh::SftpOverwriteMode>(mode)
    static_cast<void>(mode);

    RemoteJobQueue actions;

    // TODO: use QDir with filters when "ignored patterns" feature
    // is implemented
    QDir localParentDir(local.toString());
    auto entries = localParentDir.entryInfoList(QDir::AllEntries | QDir::NoDotAndDotDot);

    for (const auto &entry : entries) {
        if (entry.isFile()) {
            auto localFile = entry.filePath();
            auto remoteFile = remote.pathAppended(entry.fileName()).toString();

            qDebug() << "Queue: upload file:" << localFile << "->" << remoteFile;
            actions.enqueue([localFile, remoteFile](QSsh::SftpSession *session) {
                auto action = session->uploadFile(localFile, remoteFile);
                qDebug() << "SFTP"
                         << "*" << action << ": upload file: " << localFile << "->" << remoteFile;
                return action;
            });
        } else if (entry.isDir()) {
            auto localDir = entry.filePath();

            qDebug() << "Queue: upload directory:" << localDir << "->" << remote.toString();
            actions.enqueue([localDir, remote](QSsh::SftpSession *channel) {
                // TODO: check if this works
                auto action = channel->uploadFile(localDir, remote.toString());
                qDebug() << "SFTP"
                         << "*" << action << ": upload directory" << localDir << "->"
                         << remote.toString();
                return action;
            });
        } else {
            qDebug() << "Queue: unsupported file type for: " << entry.fileName();
        }
    }

    return actions;
}

RemoteJobId SftpConnection::createJob(RemoteJobQueue *actions)
{
    if (!actions)
        return REMOTE_INVALID_JOB;

    auto jobId = createJobId();
    while (m_actions.contains(jobId))
        jobId = createJobId();

    m_actions.insert(jobId, actions);

    if (m_ssh.state() == QSsh::SshConnection::Connected) {
        startJobs();
    } else {
        m_ssh.connectToHost();
    }

    return jobId;
}

QString SftpConnection::errorString() const
{
    return m_ssh.errorString();
}

} // namespace RemoteDev

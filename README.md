Building
========

The steps are different when building from source and when building using pre-built binaries from Qt Maintenance Tool (i.e. "Qt Creator X.Y.Z Debug Symbols" and "Qt Creator X.Y.Z Plugin Development")

## Using Pre-built Binaries

Before configuring build in QtCreator (or running qmake from CLI) set the following environment variables:

```sh
export QTC_SOURCE=${YOUR_QT_BASEDIR}/Tools/Tools/QtCreator/include/qtcreator
export QTC_BUILD=${YOUR_QT_BASEDIR}/Tools/QtCreator
```

## Using Built From Source Qt Creator

Before configuring build in QtCreator (or running qmake from CLI) set the following environment variable:

```sh
export QTC_SOURCE=${YOUR_QT_CREATOR_SOURCE_DIR}
export QTC_SOURCE=${YOUR_QT_CREATOR_BUILD_DIR}
```

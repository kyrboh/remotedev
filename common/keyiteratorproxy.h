#pragma once

namespace RemoteDev::Common {

//! Standardize key_iterator interface
//!
//! This allows to iterate over keys of a container with the key_iterator
//! interface (like QHash) using the range-based for
template <typename Container>
class KeyIteratorProxy {
public:
    explicit KeyIteratorProxy(const Container& container) : m_container{container} {}

    auto begin() const { return m_container.keyBegin(); }
    auto end() const { return m_container.keyEnd(); }

private:
    const Container& m_container;
};

} // namespace RemoteDev::Common
